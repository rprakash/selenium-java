package com.dkatalis.demo.tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import com.dkatalis.demo.apiUtils.AddEntryInHashMapParallaly;
import com.dkatalis.demo.apiUtils.ReadTestDataParallaly;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

//Problem Statement 2 | API Compare Utility

/**
 * Java test class for API test assignment.
 * NOTE: Since we are going to have millions of request so I think we should use thread parallel execution. However I found some limitations/issues in TestNG.
 * Found that TestNG has some limitations/bug working with multiple threads. Hence using plain java to implement the solution to run parallelly.
 *
 */
public class ApiTestClass {
	public static HashMap<String, String> hmInput1 = new HashMap<String, String>();
	public static HashMap<String, String> hmInput2 = new HashMap<String, String>();
	
	public static HashMap<String, String> hmOutput1 = new HashMap<String, String>();
	public static HashMap<String, String> hmOutput2 = new HashMap<String, String>();
	Response response1;
	Response response2;

	static String file1 = "C:\\Users\\Admin\\eclipse-workspace\\dkatalis-test\\src\\test\\resources\\api-test-data1.txt";
	static String file2 = "C:\\Users\\Admin\\eclipse-workspace\\dkatalis-test\\src\\test\\resources\\api-test-data2.txt";

	/**
	 * Load all requests url from files into HashMap and assigne key based on their Line number in the file
	 *
	 * @param objReader instance of BufferedReader
	 * @param fileName String File from which data will be loaded into HashMap
	 */
	public void populateHashMap(BufferedReader objReader, String fileName) {
		String strCurrentLine = null;
		int i = 1;
		int j = 1;
		try {
			while ((strCurrentLine = objReader.readLine()) != null) {
				if (fileName.contains("data1.txt")) {
					if (hmInput1.isEmpty()) {
						hmInput1.put("line" + "_" + i, strCurrentLine);
					} else {
						i = i + 1;
						hmInput1.put("line" + "_" + i, strCurrentLine);
					}
				} else {
					if (hmInput2.isEmpty()) {
						hmInput2.put("line" + "_" + j, strCurrentLine);
					} else {
						j = j + 1;
						hmInput2.put("line" + "_" + j, strCurrentLine);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create HashMap to hold responses and use line number(from inputHm) as the key to fetch corresponding values from outputHm
	 *
	 * @param request RequestSpecification
	 * @param inputHm HashMap from where the request url will be taken
	 * @param outputHm HashMap where the response will be storder
	 */
	public void MakeEnteryInOutputResMap(RequestSpecification request, HashMap<String, String> inputHm,
			HashMap<String, String> outputHm) {
		Iterator<Map.Entry<String, String>> itr = inputHm.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry pair = (Map.Entry) itr.next();
			Response res = request.get(pair.getValue().toString());
			String key = pair.getKey().toString();
			outputHm.put(key, res.asString());
		}
	}

	/**
	 * checks for quality of 2 hashMaps where responses are saved as values of HashMap
	 *
	 * @return Boolean true if equal else false
	 */
	public static void verifyResponse() {
		ArrayList<String> keys = hmOutput1.keySet().stream().collect(Collectors.toCollection(ArrayList::new));
		
		keys.forEach((key) -> {
	          
	          System.out.print(key + " ");
	          String resKey1=hmOutput1.get(key);
				String resKey2=hmOutput2.get(key);
				if(resKey1.equals(resKey2)) {
					System.out.println(hmInput1.get(resKey2) + " equals "+ hmInput2.get(resKey2));
				}
				else {
					System.out.println(hmInput1.get(resKey2) + " not equals "+ hmInput2.get(resKey2));
				}
	        });
	}

	//Found that TestNG has some limitations/bug working with multiple threads. Hence using plain java to implement the solution to run parallelly
//	@BeforeTest
//	public void loadData() throws FileNotFoundException, InterruptedException {
//
//		// Single object of BufferedReader will read two files.
//		BufferedReader br = null;
//		ReadTestDataParallaly r1 = new ReadTestDataParallaly(file1, br);
//		ReadTestDataParallaly r2 = new ReadTestDataParallaly(file2, br);
//		r1.start();
//		r2.start();
//	}


	//Found that TestNG has some limitations/bug working with multiple threads. Hence using plain java to implement the solution to run parallelly
	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		BufferedReader br = null;
		//Read files parallaly
		ReadTestDataParallaly r1 = new ReadTestDataParallaly(file1, br);
		ReadTestDataParallaly r2 = new ReadTestDataParallaly(file2, br);
		r1.start();
		r2.start();
		
		//Create output HashMap entry(response) parallaly
		RequestSpecification request = null;
		AddEntryInHashMapParallaly nc1 = new AddEntryInHashMapParallaly(request, hmInput1, hmOutput1);
		AddEntryInHashMapParallaly nc2 = new AddEntryInHashMapParallaly(request, hmInput2, hmOutput2);
		nc1.start();
		nc2.start();
		System.out.println(Thread.activeCount());
		//wait for the other threads to complete previous task before validation.In the end only main thread will be active
		while (Thread.activeCount() != 1) {
			Thread.sleep(1);
		}
		//Verify the result and print in console
		verifyResponse();
	}
}
