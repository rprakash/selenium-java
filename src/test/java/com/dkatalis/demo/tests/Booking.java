package com.dkatalis.demo.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.dkatalis.demo.pages.HomePage;
import com.dkatalis.demo.pages.OrderSummaryPage;
import com.dkatalis.demo.pages.PaymentPage;
import com.dkatalis.demo.pages.ShoppingCartPage;
import com.dkatalis.demo.webUtils.TestBase;

//Problem Statement 1 | UI Test Automation
//
//1. Create an automation test script to test end to end checkout flow for purchasing
//�Pillow� using Credit Card as payment method. This should be a SUCCESSFUL
//payment flow.
//2. Create the second script to test end to end checkout flow for purchasing �Pillow�
//using Credit Card as payment method. This should be FAILED payment flow.
//3. Implement a reporting framework of your choice to display the test results
//4. The above tests should be cross-browser compatible on Chrome and Firefox

public class Booking extends TestBase {
	private String homePageUrl = "https://demo.midtrans.com/";
	private HomePage homePage;
	private ShoppingCartPage shoppingCartPage;
	private OrderSummaryPage orderSummaryPage;
	private PaymentPage paymentPage;

	@BeforeClass
	public void instantiateObject() {
		homePage = PageFactory.initElements(driver, HomePage.class);
		shoppingCartPage = PageFactory.initElements(driver, ShoppingCartPage.class);
		orderSummaryPage = PageFactory.initElements(driver, OrderSummaryPage.class);
		paymentPage = PageFactory.initElements(driver, PaymentPage.class);
	}

	@Test(dataProvider = "objectTestData")
	public void placeOrder(TestData data) {
		String qty = "1000";
		String crdNumber = data.get(0);
		String expDate = data.get(1);
		String cvvNumber = data.get(2);
		String otp = data.get(3);
		String shouldBeSuccessful = data.get(4);
		driver.get(homePageUrl);
		homePage.buyPillow();
		shoppingCartPage.selectQntity(qty);
		shoppingCartPage.checkout();
		orderSummaryPage.continueOrderCheckout();
		paymentPage.payAmount(crdNumber, expDate, cvvNumber, otp);
		Assert.assertTrue(paymentPage.isPaymentSuccessful(shouldBeSuccessful), "Payment is not successful");
	}

	@DataProvider(name = "objectTestData")
	public static Object[][] objectTestData() {
		return new Object[][] { { new TestData("4811 1111 1111 1114", "12/24", "123", "112233", "true") },
							  { new TestData("4911 1111 1111 1113", "02/22", "123", "112233", "false") } };
	}

	static class TestData {
		public String[] items;

		public TestData(String... items) {
			this.items = items; // should probably make a defensive copy
		}

		public String get(int x) {
			return items[x];
		}
	}
}
