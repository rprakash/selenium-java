package com.dkatalis.demo.apiUtils;

import java.util.HashMap;

import com.dkatalis.demo.tests.ApiTestClass;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class AddEntryInHashMapParallaly extends Thread {
//	HashMap<String, String> req1 = null;
	HashMap<String, String> hasMap1 = null;
	HashMap<String, String> hasMap2;
	ApiTestClass p;
	RequestSpecification request1,request2;

	public AddEntryInHashMapParallaly(RequestSpecification request, HashMap<String, String> inputHashMp, HashMap<String, String> outputHashMp) {
//		this.req1 = req1;
//		this.req2 = req2;
		this.hasMap1 = inputHashMp;
		this.hasMap2 = outputHashMp;
		p = new ApiTestClass();
		request1=request;
		request2 = RestAssured.given();
	}
	
	@Override
	public void run() {
		request1=request2;
		p.MakeEnteryInOutputResMap(request1, hasMap1, hasMap2);
	}
}
