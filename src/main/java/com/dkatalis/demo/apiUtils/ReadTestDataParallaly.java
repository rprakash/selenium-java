package com.dkatalis.demo.apiUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.dkatalis.demo.tests.ApiTestClass;

public class ReadTestDataParallaly extends Thread {
	String filename;
	BufferedReader reader, br;
	ApiTestClass p;
	AddEntryInHashMapParallaly n;

	public ReadTestDataParallaly(String f, BufferedReader b) throws FileNotFoundException {
		filename = f;
		reader = b;
		br = new BufferedReader(new FileReader(filename));
		p = new ApiTestClass();
	}

	@Override
	public void run() {
		reader = br;
		p.populateHashMap(reader, filename);

	}
}
