package com.dkatalis.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.dkatalis.demo.webUtils.Wait;

/**
 * Models the Order Summary Page
 */
public class OrderSummaryPage {

	@FindBy(xpath = "//div[@class='button-main show']")
	WebElement continueBtn;

	WebDriver driver;
	Wait wait;

	/**
	 * Constructor
	 * @param driver webdriver instance
	 */
	public OrderSummaryPage(WebDriver driver) {
		this.driver = driver;
		wait = new Wait(driver);
	}

	/**
	 * Opens the page to checkout the order
	 */
	public void continueOrderCheckout() {
		driver.switchTo().frame("snap-midtrans");
		wait.untilElementIsVisible(continueBtn);
		continueBtn.click();
	}

}
