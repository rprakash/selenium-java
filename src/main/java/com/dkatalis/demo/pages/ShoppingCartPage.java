package com.dkatalis.demo.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.dkatalis.demo.webUtils.Wait;

/**
 * models the Shopping Cart Page
 */
public class ShoppingCartPage {
	@FindBy(xpath = "//input[@type='number']")
	WebElement inputQntityBox;

	@FindBy(xpath = "//td[@class='amount']")
	WebElement totalQty;

	@FindBy(xpath = "//div[@class='cart-checkout']")
	WebElement checkOutBtn;

	WebDriver driver;
	Wait wait;

	/**
	 * Constructor
	 * @param driver webdriver instance
	 */
	public ShoppingCartPage(WebDriver driver) {
		this.driver = driver;
		wait = new Wait(driver);
	}

	/**
	 * Types the quantiry of pillow that user wants to buy
	 * @param qty String number number of pillow that user wants to order
	 */
	public void selectQntity(String qty) {
		inputQntityBox.clear();
		inputQntityBox.sendKeys(qty);
	}

	/**
	 * Clicks on checkout button
	 */
	public void checkout() {
		checkOutBtn.click();
	}

	/**
	 * Checks if correct quantity(total) is displayed
	 * @param qty String expected quantity
	 * @return boolean true if correct quantity is displayed else returns false
	 */
	public boolean IsCorrectOrderQntyDisplayed(String qty) {
		try {
			return totalQty.getText().equalsIgnoreCase(qty);
		} catch (Exception e) {
			return false;
		}
	}
}
