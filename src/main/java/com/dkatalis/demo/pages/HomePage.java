package com.dkatalis.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.dkatalis.demo.webUtils.Wait;

/**
 * Models the home page
 */
public class HomePage {
	@FindBy(xpath = "//a[@class='btn buy']")
	WebElement buyNow;

	WebDriver driver;
	Wait wait;

	/**
	 * Constructor
	 * @param driver webdriver instance
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		wait = new Wait(driver);
	}

	/**
	 * Opens the page to buy pillow
	 */
	public void buyPillow() {
		wait.untilElementIsClickable(buyNow);
		buyNow.click();
	}
}
