package com.dkatalis.demo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.dkatalis.demo.webUtils.Wait;

/**
 * Models the Payment page
 */
public class PaymentPage {

	@FindBy(xpath = "//div[text()='Pay with Visa, MasterCard, JCB, or Amex']")
	WebElement CreditOrDebitCrd;

	@FindBy(name = "cardnumber")
	WebElement crdNmbr;

	@FindBy(xpath = "//input[@placeholder='MM / YY']")
	WebElement expDateBox;

	@FindBy(xpath = "//input[@inputmode='numeric']")
	WebElement cvvBox;

	@FindBy(xpath = "//a[@class='button-main-content']")
	WebElement payNowBtn;

	@FindBy(xpath = "//input[@type='password']")
	WebElement otpPwd;

	@FindBy(name = "ok")
	WebElement submitPyment;

	WebDriver driver;
	Wait wait;

	/**
	 * Constructor
	 * @param driver webdriver instance
	 */
	public PaymentPage(WebDriver driver) {
		this.driver = driver;
		wait = new Wait(driver);
	}

	/**
	 * Make payment for the placed order
	 * 
	 * @param crdNumber String credit card number
	 * @param expDate   String expiry date of credit card
	 * @param cvvNumber String cvv number of credit card
	 * @param otp       String otp sent to mobile to confirm the payment
	 */
	public void payAmount(String crdNumber, String expDate, String cvvNumber, String otp) {
		CreditOrDebitCrd.click();
		crdNmbr.sendKeys(crdNumber);
		expDateBox.sendKeys(expDate);
		cvvBox.sendKeys(cvvNumber);
		payNowBtn.click();
		switchToFrame(1);
		wait.untilElementIsVisible(otpPwd);
		otpPwd.sendKeys(otp);
		submitPyment.click();
	}

	/**
	 * Switch to the inner frame of snap-midtrans frame
	 * @param frmeIndex int index number of the inner frame
	 */
	public void switchToFrame(int frmeIndex) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("snap-midtrans");
		for (int i = 0; i < frmeIndex; i++) {
			driver.switchTo().frame(i);
		}
	}

	/**
	 * Checks for the 'transaction failed' message and returns false if displayed else return true
	 * @return Boolean true or false
	 */
	public boolean isPaymentSuccessful(String shouldBeSuccessful) {
		driver.switchTo().defaultContent();
		wait.untilFrameIsAvailableAndSwitchToIt("snap-midtrans");
		// driver.switchTo().frame("snap-midtrans");
		if (shouldBeSuccessful.equalsIgnoreCase("true")) {
			try {
				wait.untilPresenceOfElementLocated(By.xpath("//div[text()='Transaction successful']"));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			try {
				wait.untilPresenceOfElementLocated(By.xpath("//span[text()='Your card got declined by the bank']"));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}
}
