package com.dkatalis.demo.webUtils;

/**
 * Reads the different configuration options
 * It implements the singleton pattern
 * All the values retrieved are cached for later use
 */
public class Configuration {
	private static Configuration instance;
	private PropertiesFileReader fileReader;
	private String configFilePath = "src/main/resources/default.properties";

	public static Configuration getInstance() {
		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	}

    /**
     * Upon construction the configuration file is loaded
     */
	private Configuration() {
		fileReader = new PropertiesFileReader(configFilePath);
	}

    /**
     * Obtains the configuration file.
     * Uses values from the configuration file as defaults
     * and allows to be overwritten by environment properties.
     * 
     * @param key What to retrieve
     * @return Value of the specified setting
     */
	public String get(String key) {
		String envValue = System.getProperty(key);
		if (envValue != null && !envValue.isEmpty()) {
			return envValue;
		} else {
			return fileReader.get(key);
		}
	}
}
