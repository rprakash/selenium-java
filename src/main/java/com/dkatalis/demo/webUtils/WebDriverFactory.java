package com.dkatalis.demo.webUtils;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Provides methods to create drivers
 *
 */
public class WebDriverFactory {
	private static WebDriverFactory instance = null;
	private Configuration config;
	private String gridHubUrl;
	private String hubUrlPath = "/wd/hub";
	private int implicitWaitSeconds;

	public static WebDriverFactory getInstance() {
		if (instance == null) {
			instance = new WebDriverFactory();
		}
		return instance;
	}

	protected WebDriverFactory() {
		config = Configuration.getInstance();
		implicitWaitSeconds = Integer.parseInt(config.get("selenium.implicitWaitSeconds"));
		String isGridEnabled = config.get("grid.enabled");
		if (isGridEnabled != null && isGridEnabled.equals("true")) {
			gridHubUrl = config.get("grid.hub");
		}
	}

    /**
     * Creates a driver to be used with the grid
     * 
     * @param hubUrl Url where the Selenium grid hub is located
     * @return Driver instance
     */
	private WebDriver createRemoteWebDriver(String hubUrl) {
		RemoteWebDriver driver = null;
		DesiredCapabilities capabilities;
		String targetBrowser = config.get("browser.name");
		switch (targetBrowser.toLowerCase()) {
		case "firefox":
		default:
			// Firefox is used by default
			capabilities = DesiredCapabilities.firefox();
		}
		System.out.println("Starting remote driver on " + gridHubUrl + " for execution on " + targetBrowser);
		System.out.println("Starting remote driver on " + gridHubUrl);
		try {
			driver = new RemoteWebDriver(new URL(hubUrl), capabilities);
			driver.setFileDetector(new LocalFileDetector());
		} catch (Exception e) {
			System.err.println("Error starting remote driver: " + e.getMessage());
			System.exit(-1);
		}
		return driver;
	}

    /**
     * Creates a driver to be used in local executions, based on the configuration settings
     * 
     * @return Driver instance
     */
	private WebDriver createLocalWebDriver() {
		WebDriver driver;
		Configuration config = Configuration.getInstance();
		String targetBrowser = config.get("browser.name");
		implicitWaitSeconds = Integer.parseInt(config.get("selenium.implicitWaitSeconds"));

		System.out.println("Running locally using " + targetBrowser);
		switch (targetBrowser.toLowerCase()) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", BrowserUtils.getChromeDriverPath());
			driver = new ChromeDriver();
			break;
		case "firefox":
		default:
			System.setProperty("webdriver.gecko.driver", BrowserUtils.getFirefoxDriverPath());
			driver = new FirefoxDriver();
			break;
		}

		return driver;
	}

    /**
     * Get the current driver
     * 
     * @return Driver used by all the classes
     */
	public WebDriver getDriver() {
		String targetBrowser = config.get("browser.name");
		WebDriver driver;
		if (gridHubUrl != null && !gridHubUrl.isEmpty()) {
			driver = createRemoteWebDriver(gridHubUrl + hubUrlPath);
		} else {
			driver = createLocalWebDriver();
		}
		driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
		if (!targetBrowser.equalsIgnoreCase("mobileapp")) {
			driver.manage().window().maximize();
		}
		return driver;
	}

    /**
     * Disable the timeout applied when looking for DOM elements.
     * 
     * IMPORTANT: remember to call enableImplicitWaits() after the assertion has been performed
     * 
     * @param driver Driver for which to disable the implicit waits
     * @see #enableImplicitWaits
     */
	public void disableImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

    /**
     * Restores the original timeout used in DOM queries
     * 
     * @param driver Driver for which to enable the implicit waits
     */
	public void enableImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
	}

}

