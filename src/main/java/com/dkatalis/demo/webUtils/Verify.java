package com.dkatalis.demo.webUtils;

import org.testng.Assert;
import org.testng.Reporter;

/**
 * Provides methods to check the verification points and log the results in the
 * report. At the same time, all verifications perform a TestNG assertion.
 */
public class Verify {
    /**
     * Asserts the condition and logs the verification point in the log
     *
     * @param condition Condition to be evaluated
     * @param description Verification point description
     */
    public static void checkPoint(boolean condition, String description) {
        description += (condition) ? ": Passed" : ": Failed";
        Assert.assertTrue(condition, description);
        Reporter.log(description);
    }
}

