package com.dkatalis.demo.webUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Provides various methods for wait *
 */
public class Wait {
	private WebDriver driver;
	private Configuration config = Configuration.getInstance();
	private int timeout = Integer.parseInt(config.get("selenium.explicitWaitSeconds"));

	public Wait(WebDriver driver) {
		this.driver = driver;
	}

    /**
     * Waits until the given element is visible
     * 
     * @param element Element which needs to be checked for visibility
     */
	public void untilElementIsVisible(WebElement element) {
		WebDriverFactory.getInstance().disableImplicitWaits(driver);
		new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
		WebDriverFactory.getInstance().enableImplicitWaits(driver);
	}

    /**
     * Ensures that an element can be clicked. It is recommended to use before calling click() when race conditions are likely to
     * happen
     * 
     * @param element Element that must be clickable
     */
	public void untilElementIsClickable(WebElement element) {
		WebDriverFactory.getInstance().disableImplicitWaits(driver);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		WebDriverFactory.getInstance().enableImplicitWaits(driver);
	}

	/**
     * Waits until the given element is present on the DOM
     * 
     * @param elementLocator Reference to the element
     */
    public void untilPresenceOfElementLocated(By elementLocator) {
        WebDriverFactory.getInstance().disableImplicitWaits(driver);
        new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(elementLocator));
        WebDriverFactory.getInstance().enableImplicitWaits(driver);
    }
 
    /**
     * Switches to a frame as soon as it becomes available
     * 
     * @param frameName Name or Id of the frame to switch to
     */
	public void untilFrameIsAvailableAndSwitchToIt(String frameName) {
		WebDriverFactory.getInstance().disableImplicitWaits(driver);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		WebDriverFactory.getInstance().enableImplicitWaits(driver);
	}
}
