package com.dkatalis.demo.webUtils;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class Listeners implements ITestListener {

	ExtentReports extent = ExtentReportGeneration.extentReportGenerator();
	ExtentTest test;

	@Override
	public void onTestStart(ITestResult result) {
		test = extent.createTest(result.getMethod().getMethodName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		test.log(Status.PASS, "Test Case Pased");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		WebDriver driver = null;
		Object currentClass = result.getInstance();
		driver = ((TestBase) currentClass).driver;

		test.fail(result.getThrowable());
		try {
		} catch (IllegalArgumentException | SecurityException e1) {
			e1.printStackTrace();
		}

		try {
			TakesScreenshot takeScreenshot = ((TakesScreenshot) driver);
			File srcFile = takeScreenshot.getScreenshotAs(OutputType.FILE);
			String destFile = System.getProperty("user.dir") + "\\reports\\" + result.getMethod().getMethodName()
					+ ".png";
			File file = new File(destFile);
			FileHandler.copy(srcFile, file);
			test.addScreenCaptureFromPath(destFile, result.getMethod().getMethodName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		extent.flush();

	}
}
