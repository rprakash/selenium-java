#Configuring the execution
You will find all the available configuration options in the default.properties file, with their default values.
default.properties provides all the necessary data to setup the framework. The same can be passed from command line as  mvn test -Dbrowser.name=chrome

#Running the framework
mvn install will install all dependencies
mvn test -Dbrowser.name=chrome

#Reports
Supports extent report and result of execution can be viewed under /reports

#NOTE:
ApiTestClass in com.dkatalis.demo.tests is java class for running api test assignment.
It needs to be run manually as java file(as of now).
Since we are going to have millions of request so we should use thread parallel execution. However there are some limitations/issues in TestNG.
Found that TestNG has some limitations/bug working with multiple threads. Hence using plain java to implement the solution to run parallelly.
A better and clean implementation of api test using TestNG is needed.

